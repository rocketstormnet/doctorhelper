﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoctorHelper.Patient
{
    public partial class PatientLogin : ContentPage
    {
        public PatientLogin()
        {
            InitializeComponent();
        }

        void Reg_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new PatientRegister());
        }

        void Login_Clicked(object sender, System.EventArgs e)
        {
            if (LoginEntry.Text.ToLower() == "login" && PasswordEntry.Text == "123")
            {
                Navigation.PushAsync(new PatientLK());
            }
        }
    }
}
