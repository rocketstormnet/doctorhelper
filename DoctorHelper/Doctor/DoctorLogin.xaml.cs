﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoctorHelper.Doctor
{
    public partial class DoctorLogin : ContentPage
    {
        public DoctorLogin()
        {
            InitializeComponent();
        }

        void Reg_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new DoctorRegister());
        }

        void Login_Clicked(object sender, System.EventArgs e)
        {
            if (LoginEntry.Text.ToLower() == "login" && PasswordEntry.Text == "123")
            {
                Navigation.PushAsync(new DoctorLK());
            }
        }
    }
}
