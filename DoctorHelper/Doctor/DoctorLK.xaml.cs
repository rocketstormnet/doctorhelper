﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace DoctorHelper.Doctor
{
    public partial class DoctorLK : ContentPage
    {
        public DoctorLK()
        {
            InitializeComponent();
        }


        void ShowPatientsPage_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new ShowPatients());
        }
    }
}
